/*--------------------------------------------------------------------
  This file is part of the han-iot-portfolio-payload-encoder-decoder.

  This code is free software:
  you can redistribute it and/or modify it under the terms of a Creative
  Commons Attribution-NonCommercial 4.0 International License
  (http://creativecommons.org/licenses/by-nc/4.0/) by
  Remko Welling (https://ese.han.nl/~rwelling/) E-mail: remko.welling@han.nl

  The program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  --------------------------------------------------------------------*/

/*!
 * \file main.cpp
 * \brief Unit test to test and develop the payload -encoder -decoder.
 * \author Remko Welling (remko.welling@han.nl)
 * \date 9-9-2022
 * \version see version table
 *
 * # Version history
 *
 * Version|Date        |Note
 * -------|------------|-----------------
 * 0.1    | 3-3-2020   | Initial version
 * 0.2    | 9-9-2022   | Clean-up of code
 *
 */

#include <iostream>

#include "mypayloaddecoder.h"
#include "mypayloadencoder.h"
#include "test_myPayloadEncoderDecoder.h"

using namespace std;

//myPayloadEncoder encoder;   ///< encoder object for normal operation
//myPayloadDecoder decoder;   ///< decoder object for normal operation

int main()
{
    // Run unit tests
    cout << "Unit test Payload -encoder -decoder." << endl;
    testGroup1();
    cout << "Unit tests executed." << endl;
    
    return 0;
}

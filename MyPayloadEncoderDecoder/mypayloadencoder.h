/*--------------------------------------------------------------------
  This file is part of the han-iot-portfolio-payload-encoder-decoder.

  This code is free software:
  you can redistribute it and/or modify it under the terms of a Creative
  Commons Attribution-NonCommercial 4.0 International License
  (http://creativecommons.org/licenses/by-nc/4.0/) by
  Remko Welling (https://ese.han.nl/~rwelling/) E-mail: remko.welling@han.nl

  The program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  --------------------------------------------------------------------*/

/*!
 * \file mypayloadencoder.h
 * \brief payloadencoder class
 * \author Remko Welling (remko.welling@han.nl)
 * \date See version table
 * \version see version table
 *
 * # Version history
 *
 * Version|Date        |Note
 * -------|------------|-----------------
 * 0.1    | 3-3-2020   | Initial version
 * 0.2    | 14-9-2020  | replaced exisiting implementenation fro one with a buffer, added setters and getters.
 *
 */

#ifndef MYPAYLOADENCODER_H
#define MYPAYLOADENCODER_H

#include <stdint.h> // uint8_t, uint16_t, and uint32_t type

const uint8_t SENSOR_PAYLOAD_SIZE = 52;     ///< Payload size for sensor

/// \brief payload endoder class
/// This class wil encode variables for the LoRaWAN application in to a single payload
/// The class is setup using both .h and .cpp files where the setters and getters are
/// placed in to the .cpp file.
class myPayloadEncoder
{
private:
    uint32_t _testVariable1;     ///
    uint16_t _testVariable2;

    uint8_t *_buffer;             ///< buffer containing payload with sensor data
    uint8_t _bufferSize;         ///< Size of payload for housekeeping.

    /// \brief add uint32 to payload
    /// \param idx_in start location in _buffer
    /// \param value uint32_t value
    /// \return First free location at which new dat acan be stored in _buffer
    unsigned char add_uint32 (unsigned char idx_in, uint32_t value);

    /// \brief add uint16 to payload
    /// \param idx_in start location in _buffer
    /// \param value uint16_t value
    /// \return First free location at which new dat acan be stored in _buffer
    unsigned char add_uint16 (unsigned char idx_in, const uint16_t value);
    
public:
    myPayloadEncoder();     ///< Constructor
    ~myPayloadEncoder();    ///< Destructor

    /// \brief "Setter" to demonstrate setting a private member variable
    /// \param[in] var variable of the type integer
    void setDemoVariable1(const int var){_testVariable1 = static_cast<uint32_t>(var);};

    /// \brief "Setter" to demonstrate setting a private member variable
    /// \param[in] var variable of the type integer
    void setDemoVariable2(const int var){_testVariable2 = static_cast<uint16_t>(var);};

    /// \brief compose payload
    /// \pre all variables that shall be added to the payload shall be set.
    void composePayload();

    /// \brief get payload size.
    /// (this is a "Getter")
    /// \return buffer size in bytes
    uint8_t getPayloadSize(){return _bufferSize;};

    /// \brief get payload.
    /// (this is a "Getter")
    /// \return pointer to buffer
    uint8_t *getPayload(){return _buffer;};
};

#endif // MYPAYLOADENCODER_H

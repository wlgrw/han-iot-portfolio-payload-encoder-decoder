/*--------------------------------------------------------------------
  This file is part of the han-iot-portfolio-payload-encoder-decoder.

  This code is free software:
  you can redistribute it and/or modify it under the terms of a Creative
  Commons Attribution-NonCommercial 4.0 International License
  (http://creativecommons.org/licenses/by-nc/4.0/) by
  Remko Welling (https://ese.han.nl/~rwelling/) E-mail: remko.welling@han.nl

  The program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  --------------------------------------------------------------------*/

/*!
 * \file test_myPayloadEncoderDecoder.h
 * \brief unit tests to test payloadencoder and payload decoder classes.
 * \author Remko Welling (remko.welling@han.nl)
 * \date 3-3-2020
 * \version see version table
 *
 * # Version history
 *
 * Version|Date        |Note
 * -------|------------|-----------------
 * 0.1    | 3-3-2020   | Initial version
 *
 *
 */

/*!

# Description of unit test.
The unit test is devided over two seperat tests that are part of one test group.
The test group is a grouyp of tests that test a specific feature of the payload
-encoder -decoder class.

Each test is using its own objects for testing. Tests are using the following schematic:

\verbatim

                   +---------+                   +---------+
                   |         |                   |         |
input variable --->| encoder |----> payload ---->| decoder |----> result
                   |         |                   |         |
                   +---------+                   +---------+

\endverbatim


 */

#ifndef TEST_MYPAYLOADENCODERDECODER_H
#define TEST_MYPAYLOADENCODERDECODER_H

/// \brief function to call unit test part of test group 1
void testGroup1();

/// \brief unit test 1
void test1();

/// \brief unit test 2
void test2();

#endif // TEST_MYPAYLOADENCODERDECODER_H
